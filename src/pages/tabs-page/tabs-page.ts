import { Component } from '@angular/core';

import { NavParams } from 'ionic-angular';

import { NewsfeedPage } from '../newsfeed/newsfeed';
import { PeoplePage } from '../people/people';
import { TicketsPage } from '../tickets/tickets';
import { TasksPage } from '../tasks/tasks';
import { MePage } from '../me/me';

@Component({
  templateUrl: 'tabs-page.html'
})
export class TabsPage {
  // set the root pages for each tab
  tab1Root: any = NewsfeedPage;
  tab2Root: any = PeoplePage;
  tab3Root: any = TicketsPage;
  tab4Root: any = TasksPage;
  tab5Root: any = MePage;
  mySelectedIndex: number;

  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

}
