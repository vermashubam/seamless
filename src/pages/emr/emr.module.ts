import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmrPage } from './emr';

@NgModule({
  declarations: [
    EmrPage,
  ],
  imports: [
    IonicPageModule.forChild(EmrPage),
  ],
})
export class EmrPageModule {}
