import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {EmrPage } from '../emr/emr';
import {ReorderPage } from '../reorder/reorder';
import {ClientTicketsPage } from '../client-tickets/client-tickets';
import {EventsPage } from '../events/events';



/**
 * Generated class for the ClientdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-clientdetails',
  templateUrl: 'clientdetails.html',
})
export class ClientdetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientdetailsPage');
  }
  emr(){
    this.navCtrl.push(EmrPage);
    
  }
  reorder(){
    this.navCtrl.push(ReorderPage);
    
  }
  clienttickets(){
    this.navCtrl.push(ClientTicketsPage);
    
  }
  events(){
    this.navCtrl.push(EventsPage);
    
  }
}
