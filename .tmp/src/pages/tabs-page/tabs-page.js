var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { NewsfeedPage } from '../newsfeed/newsfeed';
import { PeoplePage } from '../people/people';
import { TicketsPage } from '../tickets/tickets';
import { TasksPage } from '../tasks/tasks';
import { MePage } from '../me/me';
var TabsPage = /** @class */ (function () {
    function TabsPage(navParams) {
        // set the root pages for each tab
        this.tab1Root = NewsfeedPage;
        this.tab2Root = PeoplePage;
        this.tab3Root = TicketsPage;
        this.tab4Root = TasksPage;
        this.tab5Root = MePage;
        this.mySelectedIndex = navParams.data.tabIndex || 0;
    }
    TabsPage = __decorate([
        Component({template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/tabs-page/tabs-page.html"*/'<ion-tabs [selectedIndex]="mySelectedIndex" name="conference">\n  <ion-tab [root]="tab1Root" tabTitle="Newsfeed" tabIcon="calendar" tabUrlPath="conference-schedule"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="People" tabIcon="contacts"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Tickets" tabIcon="map"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="Tasks" tabIcon="information-circle"></ion-tab>\n  <ion-tab [root]="tab5Root" tabTitle="Me" tabIcon="information-circle"></ion-tab>\n  \n</ion-tabs>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/tabs-page/tabs-page.html"*/
        }),
        __metadata("design:paramtypes", [NavParams])
    ], TabsPage);
    return TabsPage;
}());
export { TabsPage };
//# sourceMappingURL=tabs-page.js.map