import { Component } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';


import { PopoverPage } from '../about-popover/about-popover';

@NgModule({
  imports: [
   
    HttpClientModule
  ],
})

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  conferenceDate = '2017-10-10';

  constructor(public popoverCtrl: PopoverController) { }

  presentPopover(event: Event) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({ ev: event });
  }

onsplit(){

   
     
 
    
}

}
