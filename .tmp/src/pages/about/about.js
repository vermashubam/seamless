var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { PopoverPage } from '../about-popover/about-popover';
var AboutPage = /** @class */ (function () {
    function AboutPage(popoverCtrl) {
        this.popoverCtrl = popoverCtrl;
        this.conferenceDate = '2017-10-10';
    }
    AboutPage.prototype.presentPopover = function (event) {
        var popover = this.popoverCtrl.create(PopoverPage);
        popover.present({ ev: event });
    };
    AboutPage.prototype.onsplit = function () {
    };
    AboutPage = __decorate([
        NgModule({
            imports: [
                HttpClientModule
            ],
        }),
        Component({
            selector: 'page-about',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>About</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="presentPopover($event)">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="about-header">\n    <img src="assets/img/ionic-logo-white.svg" alt="ionic logo">\n  </div>\n  <div padding class="about-info">\n    <h4>Ionic Conference</h4>\n\n  \n\n    <ion-list no-lines>\n        >\n\n      <ion-item>\n        <ion-icon name="calendar" item-start></ion-icon>\n        <ion-label>SELECT DATE</ion-label>\n        <ion-datetime displayFormat="MMM DD, YYYY" max="2056" [(ngModel)]="conferenceDate"></ion-datetime>\n      </ion-item>\n\n     <ion-item>\n      <ion-label stacked color="primary">AMOUNT</ion-label>\n       <ion-input type="text" aria-placeholder="amount spent..." [(ngModel)]="amount" required></ion-input>\n     </ion-item>\n\n     <ion-item>\n      <button ion-button (click)="onsplit()" type="submit" block>POST</button>\n     </ion-item>\n\n    </ion-list>\n\n    <p>\n     {{amount}}\n    </p>\n  </div>\n\n  <div class="animatebox">\n    \n  </div>\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [PopoverController])
    ], AboutPage);
    return AboutPage;
}());
export { AboutPage };
//# sourceMappingURL=about.js.map