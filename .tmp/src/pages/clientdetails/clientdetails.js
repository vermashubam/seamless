var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EmrPage } from '../emr/emr';
import { ReorderPage } from '../reorder/reorder';
import { ClientTicketsPage } from '../client-tickets/client-tickets';
import { EventsPage } from '../events/events';
/**
 * Generated class for the ClientdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClientdetailsPage = /** @class */ (function () {
    function ClientdetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ClientdetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ClientdetailsPage');
    };
    ClientdetailsPage.prototype.emr = function () {
        this.navCtrl.push(EmrPage);
    };
    ClientdetailsPage.prototype.reorder = function () {
        this.navCtrl.push(ReorderPage);
    };
    ClientdetailsPage.prototype.clienttickets = function () {
        this.navCtrl.push(ClientTicketsPage);
    };
    ClientdetailsPage.prototype.events = function () {
        this.navCtrl.push(EventsPage);
    };
    ClientdetailsPage = __decorate([
        Component({
            selector: 'page-clientdetails',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/clientdetails/clientdetails.html"*/'<!--\n  Generated template for the ClientdetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>clientdetails</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<style>\n    .bottom-section {\n      position: inherit;\n    }\n    body {\n      padding-top: 72px!important;\n    }\n    body{\n    padding:0;\n    }\n    .client-details {\n      padding: 8px 12px 0;\n    }\n    .clientt h4 {\n      margin: 11px 0 0;\n    }\n    .search-btn {\n      top: -42px;\n      right: -3px;\n    }\n    .search .text-field.show-field {\n      margin-bottom: 12px;\n    }\n    .upload-icon.common-class > button {\n        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;\n        border: medium none;\n        outline: medium none;\n    }\n    .search input[type="text"] {\n      right: 11px;\n      width: 96%;\n    }\n    .upload-icon.common-class > input {\n      background-color: rgba(0, 0, 0, 0);\n      background-image: url("images/upload-icon.png");\n      background-repeat: no-repeat;\n      background-size: 63% auto;\n      height: 30px;\n      position: relative;\n      right: 0;\n      text-indent: -999px;\n      width: 30px;\n      z-index: 2147483647;\n    }\n    .img_name.upload .text.common-class.linda_photo {\n      text-align: center;\n      width: 54%!important;\n    }\n    </style>\n    \n\n\n<ion-content padding>\n\n  \n<section class="members details conditions">\n    <div class="members-access account-members-access">\n    <div class="img_name upload">\n    <div class="img common-class linda_profile"><img src="images/linda-big.png"></div>\n    <div class="text common-class linda_photo"><h4 class="take">Take Picture / Upload</h4></div>\n    <!-- <div class="upload-icon common-class"><button type="button" data-toggle="modal" data-target="#myModal"><img src="images/upload-icon.png"></button></div> -->\n    <div class="upload-icon common-class"><input type="file"></div>\n    <div id="myModal" class="modal fade" role="dialog" style="display: none;">\n      <div class="modal-dialog">\n    \n        <!-- Modal content-->\n        <div class="modal-content">\n          <div class="modal-header">\n         <h4 class="modal-title">Select image</h4>\n            <button type="button" class="close" data-dismiss="modal">×</button>\n           \n          </div>\n          <div class="modal-body">\n            <input type="file">\n        \n          </div>\n        <div class="modal-footer">\n            <div class="upload_btn"><a class="upload_img" href="javascript:;">Uplode image</a></div>\n            <div class="clearfix"></div>\n            <p class="display_on_click" style="display:none;">Your image has been uploaded successfully.</p>\n          </div>\n        </div>\n    \n      </div>\n    </div>\n    </div>\n    \n    \n    <div class="img_name upload with_name common-text">\n    <div class="img common-class"><h4>Name</h4></div>\n    <div class="text common-class"><h4 class="take">Linda Lane</h4></div>\n    </div>\n    \n    <div class="img_name upload with_name email common-text">\n    <div class="img common-class"><h4>Allergies</h4></div>\n    <div class="text common-class"><h4 class="take">Peanut Butter, Corn</h4></div>\n    </div>\n    \n    <div class="img_name upload with_name password common-text">\n    <div class="img common-class"><h4>Conditions</h4></div>\n    <div class="text common-class"><h4 class="take">Diabetes, High Cholesterol</h4></div>\n    </div>\n    \n    <div class="img_name upload with_name note common-text">\n    <div class="img common-class"><h4>Note</h4></div>\n    <div class="text common-class"><h4 class="take">Color blind, take care of when givin.</h4></div>\n    </div>\n    </div>\n    \n    </section>\n\n\n    <div class="icons-order-section">\n        <div class="col-xs-4 col-sm-3 m-b-20px" >\n          <a (click)="emr()" class="gry-color"><img src="http://139.59.3.57/psd/seamless/images/cloc.png">\n          <h2>eMARS</h2></a>\n        </div>\n        <div class="col-xs-4 col-sm-3 m-b-20px">\n          <a (click)="reorder()" class="ion-clock">\n          <h2>ReOrder</h2></a>\n        </div>\n        <div class="col-xs-4 col-sm-3 m-b-20px">\n          <a (click)="clienttickets()" class="ion-ios-chatboxes">\n          <h2>Tickets</h2></a>\n        </div>\n        <div class="col-xs-4 col-sm-3 m-b-20px">\n          <a (click)="events()" class="ion-document">\n          <h2>Documents**</h2></a>\n        </div>\n        <div class="col-xs-4 col-sm-3 m-b-20px">\n          <a (click)="events()" class="ion-ios-information-outline">\n          <h2>Events</h2></a>\n        </div>\n        <div class="col-xs-4 col-sm-3 m-b-20px">\n          <a (click)="emr()" class="gry-color"><img src="images/calander.png">\n          <h2>Appoinments**</h2></a>\n        </div>\n        <div class="col-xs-4 col-sm-3">\n          <a (click)="emr()" class="gry-color"><img src="images/phar.png">\n          <h2>Pharmacist Corner**</h2></a>\n        </div>\n        <div class="col-xs-4 col-sm-3">\n          <a (click)="emr()" class="gry-color"><img src="images/graph.png">\n          <h2>Health Graphs**</h2></a>\n        </div>\n      </div>\n      \n    \n\n  \n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/clientdetails/clientdetails.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], ClientdetailsPage);
    return ClientdetailsPage;
}());
export { ClientdetailsPage };
//# sourceMappingURL=clientdetails.js.map