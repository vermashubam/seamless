import { Component , OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { Http, Response } from '@angular/http';


/**
 * Generated class for the MePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-me',
  templateUrl: 'me.html',
})
export class MePage implements OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams , private http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MePage');
  }

  ngOnInit(){

  }
upload(event){
 let elem = event.target;
 if(elem.files.length > 0){
   console.log(elem.files[0]);
   let formData = new FormData();
   formData.append('file' , elem.files[0]);

   this.http.post('http://localhost/chat.php', formData).subscribe((data) => {
     console.log('Gotn some data' , data)
   }, (error) => {
     console.log('Error falls' , error);
   })

 }


 
}

send(){
 this.sender="SHUBAM";
 

  
}
sender="";


  getUser() {
    return this.http.get(`https://conduit.productionready.io/api/profiles/eric`)
    .map((res:Response) => res.json());
  }

}
