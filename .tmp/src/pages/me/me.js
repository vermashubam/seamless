var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
/**
 * Generated class for the MePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MePage = /** @class */ (function () {
    function MePage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.sender = "";
    }
    MePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MePage');
    };
    MePage.prototype.ngOnInit = function () {
    };
    MePage.prototype.upload = function (event) {
        var elem = event.target;
        if (elem.files.length > 0) {
            console.log(elem.files[0]);
            var formData = new FormData();
            formData.append('file', elem.files[0]);
            this.http.post('http://localhost/chat.php', formData).subscribe(function (data) {
                console.log('Gotn some data', data);
            }, function (error) {
                console.log('Error falls', error);
            });
        }
    };
    MePage.prototype.send = function () {
        this.sender = "SHUBAM";
    };
    MePage.prototype.getUser = function () {
        return this.http.get("https://conduit.productionready.io/api/profiles/eric")
            .map(function (res) { return res.json(); });
    };
    MePage = __decorate([
        Component({
            selector: 'page-me',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/me/me.html"*/'<!--\n  Generated template for the MePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>me</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<style>\n  body{\n  padding:0px;\n  }\n  .img_name.with_name h4 {\n    font-size: 18px;\n  }\n  body {\n    padding-top: 39px;\n  }\n  .upload-icon.common-class > button {\n    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;\n    border: medium none;\n     outline: medium none;\n  }\n  .img_name.upload.with_name.password.common-text > input {\n    border: 1px solid;\n    border-radius: 7px;\n    color: #c5c5c5;\n    float: left;\n    font-family: sans-serif;\n    font-size: 19px;\n    height: 43px;\n    padding-left: 8px;\n    text-transform: capitalize;\n    width: 50%;\n  }\n  .text.common-class.sub-btn > a {\n    float: left;\n    width: 100%;\n  }\n  .img_name.upload.common-text .text.common-class.sub-btn {\n    float: inherit;\n    margin: 0 auto 0 23px;\n    width: 91%!important;\n  }\n  .text.common-class > a {\n    margin: 0;\n  }\n  .upload-icon.common-class > input {\n    background-color: rgba(0, 0, 0, 0);\n    background-image: url("images/upload-icon.png");\n    background-repeat: no-repeat;\n    background-size: 63% auto;\n    height: 30px;\n    position: relative;\n    right: 0;\n    text-indent: -999px;\n    width: 30px;\n    z-index: 2147483647;\n  }\n  .img_name.upload .text.common-class.linda_photo {\n    text-align: center;\n    width: 54%!important;\n  }\n  \n  </style>\n  \n\n\n<ion-content padding>\n\n    <div>\n        <label>SENDER : </label><input type="text" [(ngModel)]="sender" >\n        <label>RECEIVER : </label><input type="text" name="receiver" >\n        <label>MESSAGE : </label><input type="text" name="message" >\n        <input type="submit" value="submit" (click)="send()" > \n      </div>\n      <br><br>\n\n      <div>\n        <input type="file"><input type="submit" (click)="upload($event)">\n      </div>\n\n\n    <section class="members details">\n        <div class="members-access">\n        <div class="img_name upload">\n        <div class="img common-class linda_profile"><img src="images/linda-big.png" /></div>\n        <div class="text common-class linda_photo"><h4 class="take">Take Picture / Upload</h4></div>\n        <!-- <div class="upload-icon common-class"><button type="button" data-toggle="modal" data-target="#myModal"><img src="images/upload-icon.png" /></button></div> -->\n        <div class="upload-icon common-class"><input type="file"></div>\n\n        </div>\n        \n        \n        <div class="img_name upload with_name common-text">\n        <div class="img common-class"><h4>Name</h4></div>\n        <div class="text common-class"><input type="text"></div>\n        </div>\n        \n        <div class="img_name upload with_name email common-text">\n        <div class="img common-class"><h4>Email</h4></div>\n        <div class="text common-class"><h4 class="take"><input type="text"></h4></div>\n        </div>\n        \n        <div class="img_name upload with_name password common-text">\n        <div class="img common-class"><h4>Password</h4></div>\n        <div class="text common-class"><a href="javascript:;" class="pas-btn">Edit</a>\n          <a class="button icon-right icon ion-ios7-paper-outline button-clear button-dark">News</a>\n        </div>\n        </div>\n        \n        <div class="pass-sec" style="display:none;">\n        <div class="img_name upload with_name password common-text">\n        <div class="img common-class"><h4>Old Password</h4></div>\n        <div class="text common-class"><input type="text" class="old pass" placeholder="Enter Old Password"></div>\n        </div>\n        <div class="img_name upload with_name password common-text">\n        <div class="img common-class"><h4>New Password</h4></div>\n        <div class="text common-class"><input type="text" class="old pass" placeholder="Enter New Password"></div>\n        </div>\n        <div class="img_name upload with_name password common-text sub-btnn account_success">\n        <div class="text common-class sub-btn"><a href="javascript:;" class="suces-btn">Submit</a></div>\n        \n  \n        \n        \n        </div>\n        <div class="img_name upload with_name password common-text message-sucee" style="display:none;">\n        <div class="text common-class sub-btn"><p>The password has been updated successfully.</p></div>\n        </div>\n        </div>\n        \n        <div class="img_name upload with_name password common-text">\n        <div class="img common-class"><h4>Email Med Changes</h4></div>\n        <div class="text common-class switch-icon">\n        <div class="switch">\n        <form>\n        <label class="switch">\n          <input type="checkbox">\n          <span class="slider round"></span>\n        </label>\n        </form>\n        </div>\n        </div>\n        </div>\n        \n        <div class="img_name upload with_name password common-text">\n        <div class="img common-class"><h4>Email New Tickets</h4></div>\n        <div class="text common-class switch-icon">\n        <div class="switch">\n        <form>\n        <label class="switch">\n          <input type="checkbox">\n          <span class="slider round"></span>\n        </label>\n        </form>\n        </div>\n        </div>\n        </div>\n        \n        </div>\n        </section>\n        \n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/me/me.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams, Http])
    ], MePage);
    return MePage;
}());
export { MePage };
//# sourceMappingURL=me.js.map