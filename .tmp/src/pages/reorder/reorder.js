var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the ReorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReorderPage = /** @class */ (function () {
    function ReorderPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ReorderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReorderPage');
    };
    ReorderPage = __decorate([
        Component({
            selector: 'page-reorder',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/reorder/reorder.html"*/'<!--\n  Generated template for the ReorderPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>reorder</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <section class="members details team-member">\n        <div class="members-access">\n        <div class="img_name upload" style="border:none;">\n        <div class="img common-class record-hdr-img"><img src="images/linda-big.png"></div>\n        <div class="text common-class record-hdr-title"><h4 class="take">Linda L\'s<br>Medication</h4></div>\n        </div>\n        \n        <div class="reorder-title-border">\n        <div class="box-background">\n        <h4>ReOrders</h4>\n        </div>\n        </div>\n        <div class="img_name upload with_name met_formin loraze_pam">\n        <div class="img common-class"><div class="check-box"><input type="checkbox" class="okay-btn" name="tst" /><div class="check"></div></div><h4>Lorazepam 2 mg</h4><p class="reorder-par-txt">Dr.A. Homested<br>Take one table by mouth<br> twice daily for 10 days</p></div>\n        <div class="text common-class"><a href="javascript:;">...</a></div>\n        </div>\n        <div class="img_name upload with_name met_formin loraze_pam">\n        <div class="img common-class"><div class="check-box"><input type="checkbox" class="okay-btn" name="tst" /><div class="check"></div></div><h4>Tylenol 3, Take one tablet</h4><p class="reorder-par-txt">Daily when needed</p></div>\n        <div class="text common-class"><a href="javascript:;">...</a></div>\n        </div>\n        \n        <div class="reorder">\n        <a href="javascript:;">ReOrders</a>\n        </div>\n        <div class="reorder-title-border">\n        <div class="box-background">\n        <h4>Auto Fill</h4>\n        </div>\n        </div>\n        \n        \n        <div class="img_name upload with_name met_formin loraze_pam loraze_pamm">\n        <div class="img common-class"><h4 class="re-order-title"><b>Lorazeoam 2 mg</b><br>Every 30 days<br>Coming in 2 days</h4><button class="no-btn">7</button></div>\n        <div class="text common-class"><label class="switch">\n          <input type="checkbox">\n          <span class="slider round"></span>\n        </label></div>\n        </div>\n        <div class="img_name upload with_name met_formin loraze_pam loraze_pamm">\n        <div class="img common-class"><h4 class="re-order-title"><b>Tylenol 3</b><br>30 tabs Every 15 days<br>Comeing in 25 days</h4><button class="no-btn">30</button></div>\n        <div class="text common-class"><label class="switch">\n          <input type="checkbox">\n          <span class="slider round"></span>\n        </label></div>\n        </div>\n        \n        \n        <div class="reorder reorderr">\n        <a href="javascript:;">Request Autofill Change</a>\n        </div>\n        </div>\n        </section>\n        \n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/reorder/reorder.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], ReorderPage);
    return ReorderPage;
}());
export { ReorderPage };
//# sourceMappingURL=reorder.js.map