var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NewTicketPage } from '../new-ticket/new-ticket';
/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EventsPage = /** @class */ (function () {
    function EventsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EventsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventsPage');
    };
    EventsPage.prototype.newticket = function () {
        this.navCtrl.push(NewTicketPage);
    };
    EventsPage = __decorate([
        Component({
            selector: 'page-events',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/events/events.html"*/'<!--\n  Generated template for the EventsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>events</ion-title>\n  </ion-navbar>\n\n  <div class="client tickets_head newhead"><div class="clint-img"><img src="images/linda-big.png"></div><h4 class="medi-title">Linda<br>Documents</h4></div>\n  <div class="client plus"><a (click)="newticket()"><span class="ion ion-android-add">ADD</span></a></div>\n  \n\n</ion-header>\n\n\n<ion-content padding>\n\n    \n        <div class="tab-content tasks_ticket no-padding">\n            <section class="client-ticket-members">\n        \n        <div class="document-section">\n            <ul class="cal-bar">\n                <img src="http://139.59.3.57/psd/seamless/images/events-graph.png" class="img-responsive">\n              </ul>\n              <ul>\n              <li class="ion-android-checkmark-circle">SleepEvent</li>\n              <li><img src="images/black-icon.png">Menstruaurin</li>\n              <li><img src="images/green-icon.png">PRN</li>\n              <li><img src="images/black-icon.png">Urin Discharge</li>\n              <li><img src="images/blue-icon.png">Emerg Visit</li>\n              <li><img src="images/purple-icon.png">Seizure</li>\n              <li class="unique"><img src="images/brown-icon.png"><span>Bowel Movement</span></li>\n              <li><img src="images/black-icon.png">Behavior</li>\n              <li><img src="images/black-icon.png">Falls</li>\n              </ul>\n           </div>   \n              <div class="evnts-dtls">\n                <h3>Events Details</h3>\n              </div>\n\n\n              <div class="tickets">\n                <div class="img_ticket">\n              <img src="http://139.59.3.57/psd/seamless/images/gracia.png">\n              </div>\n              <a href="javascript:void(0);" class="client-tickets-click">\n              <div class="text_ticket">\n              <h3>Fall </h3>\n              <span class="days">1 day ago</span>\n              <p>The client fell while trying to go upstairs he seems fine but I will monitoe for..</p>\n              </div>\n              </a>\n              </div>\n\n\n              <div class="tickets">\n                <div class="img_ticket">\n                <img src="http://139.59.3.57/psd/seamless/images/gracia.png">\n                </div>\n              <a href="javascript:void(0);" class="client-tickets-click">\n              <div class="text_ticket">\n              <h3>Seizure</h3>\n              <span class="days">1 monts ago</span>\n              <p>Client experience a seizure today and lasted for 10 seconds.</p>\n              </div>\n              </a>\n              </div>\n\n\n\n\n              \n              </section>\n              </div>\n                      \n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/events/events.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], EventsPage);
    return EventsPage;
}());
export { EventsPage };
//# sourceMappingURL=events.js.map