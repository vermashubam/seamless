var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the TicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TicketsPage = /** @class */ (function () {
    function TicketsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tickets = ['user1', 'user2', 'user3'];
        this.today = Date.now();
    }
    TicketsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TicketsPage');
    };
    TicketsPage.prototype.chat = function () {
        alert(1);
    };
    TicketsPage = __decorate([
        Component({
            selector: 'page-tickets',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/tickets/tickets.html"*/'<!--\n  Generated template for the TicketsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>tickets</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <h1>HIEEEEEE</h1>\n\n    <section class="members alltickets_members">\n      \n\n\n\n        <div class="tickets">\n            <ul><li *ngFor="let ticket of tickets" (click)="chat()">\n\n        <div class="img_ticket">\n        <img src="http://139.59.3.57/psd/seamless/images/linda-big.png" />\n        </div>\n        <div class="text_ticket">\n       \n        <h3>{{ticket}}</h3>\n        <span class="days">{{today | date:\'short\'}}</span>\n        <p class="alltick-text">Latest message with BADGE of new messages to be displayed in 1 or 2 lines</p>\n        \n        </div>\n      </li></ul>\n        </div>\n\n      \n        \n\n        \n      \n        \n\n        \n        </section>\n        \n\n\n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/tickets/tickets.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], TicketsPage);
    return TicketsPage;
}());
export { TicketsPage };
//# sourceMappingURL=tickets.js.map