import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {NewTicketPage } from '../new-ticket/new-ticket';

/**
 * Generated class for the ClientTicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client-tickets',
  templateUrl: 'client-tickets.html',
})
export class ClientTicketsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientTicketsPage');
  }

  newticket(){
   
      this.navCtrl.push(NewTicketPage);
      
    
  }

}
