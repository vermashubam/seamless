var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NewTicketPage } from '../new-ticket/new-ticket';
/**
 * Generated class for the ClientTicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClientTicketsPage = /** @class */ (function () {
    function ClientTicketsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ClientTicketsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ClientTicketsPage');
    };
    ClientTicketsPage.prototype.newticket = function () {
        this.navCtrl.push(NewTicketPage);
    };
    ClientTicketsPage = __decorate([
        Component({
            selector: 'page-client-tickets',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/client-tickets/client-tickets.html"*/'<!--\n  Generated template for the ClientTicketsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n  <div class="client tickets_head newhead"><img src="http://139.59.3.57/psd/seamless/images/linda-big.png"><h4 class="medi-title">Linda l\'s<br>Medication</h4></div>\n  <div class="client plus"><a (click)="newticket()"><span class="ion ion-android-add">ADD</span></a></div>\n  \n\n</ion-header>\n\n\n<ion-content padding>\n\n    <section class="client-ticket-members">\n        <div class="tickets">\n        <div class="img_ticket">\n        <img src="images/adam.png" />\n        </div>\n        <a href="client-ticket-details.html" class="client-tickets-click">\n        <div class="text_ticket">\n        <h3 style="color:#333333;">Adam Gray</h3>\n        <span class="days">2h</span>\n        <p>This is a sample message that crosses over in two lines and goes on for a while. </p>\n        </div>\n        </a>\n        </div>\n        \n        <div class="tickets">\n        <div class="img_ticket">\n        <img src="images/gracia.png" />\n        </div>\n        <a href="client-ticket-details.html" class="client-tickets-click">\n        <div class="text_ticket">\n        <h3>Eugene Garcia</h3>\n        <span class="days">10h</span>\n        <p>This is a sample message that crosses over in two lines and goes on for a while. </p>\n        </div>\n        </a>\n        </div>\n        </section>\n        \n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/client-tickets/client-tickets.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], ClientTicketsPage);
    return ClientTicketsPage;
}());
export { ClientTicketsPage };
//# sourceMappingURL=client-tickets.js.map