import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTicketsPage } from './client-tickets';

@NgModule({
  declarations: [
    ClientTicketsPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientTicketsPage),
  ],
})
export class ClientTicketsPageModule {}
