var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the TasksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TasksPage = /** @class */ (function () {
    function TasksPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tasks = ['Give medicine', 'Give breakfast', 'Give Lunch'];
    }
    TasksPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TasksPage');
    };
    TasksPage.prototype.gettasks = function () {
        alert(1);
    };
    TasksPage = __decorate([
        Component({
            selector: 'page-tasks',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/tasks/tasks.html"*/'<!--\n  Generated template for the TasksPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>tasks</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n    <section class="members details team-member tasks_ticket">\n        <div class="tickets">\n<ul><li *ngFor="let task of tasks" (click)= "gettasks()">\n\n        <div class="img_ticket tasks-page-img-tickets">\n        <img src="http://139.59.3.57/psd/seamless/images/linda-big.png">\n        </div>\n      \n        <div class="text_ticket">\n        <h3 class="task-small-title">{{task}}</h3>\n        <p class="task-parg">Past due by 2 hours.</p>\n        <div class="check-box task-page-checkbox"><input class="okay-btn" name="tst" type="checkbox"><div class="check"></div></div>\n        </div>\n        \n        \n        \n        \n      </li></ul>\n        </div>\n        \n       \n      \n        \n        </section>\n        \n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/tasks/tasks.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], TasksPage);
    return TasksPage;
}());
export { TasksPage };
//# sourceMappingURL=tasks.js.map