var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ClientdetailsPage } from '../clientdetails/clientdetails';
/**
 * Generated class for the PeoplePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PeoplePage = /** @class */ (function () {
    function PeoplePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.clients = ['Shubam', 'Bharat', 'Ashok', 'NamesinArray'];
        this.peopledetail = ['Name 1', 'Name 2', 'Name 3', 'Name 4'];
    }
    PeoplePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PeoplePage');
    };
    PeoplePage.prototype.viewclient = function () {
        this.navCtrl.push(ClientdetailsPage);
    };
    PeoplePage = __decorate([
        Component({
            selector: 'page-people',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/people/people.html"*/'<!--\n  Generated template for the PeoplePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>people</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <section class="members forth_page">\n        <div class="home-one page_conversation">\n        <h4 class="client-hdng-title">Recent Conversations</h4>\n        <div class="conver_top-text-section">\n\n          \n        \n            <ul>\n                <li *ngFor="let client of clients" (click)="viewclient()">\n                    <div class="conver_sations">\n        <div class="image_client">\n        <img src="http://139.59.3.57/psd/seamless/images/linda-big.png">\n        </div>\n        \n        <div class="image_name">\n         <h4>{{client}}</h4>\n         <p class="client-para">SIMPLY CHAT</p>\n        </div>\n        \n      </div>\n        </li></ul>\n        \n\n\n                \n        </div></div>\n      \n        <div class="tabs">\n            <ul class="nav nav-tabs">\n              <li class="active"><a data-toggle="tab" id="#home" class="clientt-team">Clients</a></li>\n              <li><a data-toggle="tab" id="#menu1" class="team">Team</a></li>\n            </ul>\n            </div>\n\n\n            <div class="tab-content">\n                <div id="home" class="tab-pane in active">\n                <div class="home-one">\n              <h4 class="home-name home-name2"><span class="ion ion-chevron-right arrow-straight" style="display:none;"></span><span class="ion ion-chevron-down arrow-down" ></span>Home name 1</h4>\n              <h4 class="next-delivery">DELIVERY DATES</h4>\n              \n              \n              <div class="tab111">\n\n                <ul>\n<li *ngFor="let client of clients" (click)="viewclient()">\n\n                \n                \n              <div class="members-access">\n              <a href="client-details.html">\n              <div class="img_name">\n              <img src="images/eugene.png"> <h4>SHUBAM</h4>\n              </div>\n              </a>\n              </div>\n            </li>\n          </ul>\n              \n             \n              \n         \n              \n              </div>\n              </div>\n              </div>\n\n\n\n\n<div id="team" class="tab-pane fade">\n\n    <ul>\n        <li *ngFor="let client of clients" (click)="viewclient()">\n    \n    <div class="img_name">\n	<a href="team-member-convo.html"><img src="images/matthew.png"><h4>{{client}}</h4></a>\n	</div>\n   </li></ul>\n  \n    \n  </div>\n\n\n</div>\n\n\n\n\n\n\n\n\n          \n            \n      \n      \n      \n      </section>\n        \n\n  \n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/people/people.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], PeoplePage);
    return PeoplePage;
}());
export { PeoplePage };
//# sourceMappingURL=people.js.map