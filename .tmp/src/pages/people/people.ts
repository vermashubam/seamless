import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClientdetailsPage } from '../clientdetails/clientdetails';


/**
 * Generated class for the PeoplePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-people',
  templateUrl: 'people.html',
})
export class PeoplePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PeoplePage');
  }

  clients = ['Shubam', 'Bharat', 'Ashok', 'NamesinArray'];
  peopledetail = ['Name 1','Name 2', 'Name 3' , 'Name 4'];

  viewclient(){
    this.navCtrl.push(ClientdetailsPage);
  }

}
