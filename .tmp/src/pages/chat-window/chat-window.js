var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the ChatWindowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatWindowPage = /** @class */ (function () {
    function ChatWindowPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.today = Date.now();
        this.message_user1 = 'SHUBAM';
        this.newItem = "";
        this.items = "items are necc";
        this.heroName = '';
        this.heroes = [new Hero(1, 'One'), new Hero(2, 'Two')];
    }
    ChatWindowPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatWindowPage');
    };
    ChatWindowPage.prototype.newItemChanged = function (value) {
        this.newItem = value;
    };
    ChatWindowPage.prototype.send_message1 = function () {
        alert(this.newItem);
    };
    ChatWindowPage.prototype.send_message2 = function () {
        var _this = this;
        var findHero = this.heroes.find(function (hero) { return hero.name === _this.lastName; });
        if (findHero) {
            findHero.name = this.heroName;
        }
        else {
            this.heroes.push(new Hero(3, this.heroName));
        }
        this.heroName = '';
    };
    ChatWindowPage.prototype.addHero = function () {
    };
    ChatWindowPage.prototype.onEdit = function (hero) {
        this.lastName = hero.name;
        this.heroName = hero.name;
    };
    ChatWindowPage = __decorate([
        Component({
            selector: 'page-chat-window',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/chat-window/chat-window.html"*/'<!--\n  Generated template for the ChatWindowPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>chat-window</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n	<div class="person-one common-person" *ngFor="let hero of heroes">\n	    <div class="message-box">\n		<p>{{ hero.name }}\n		</p><button (click)="onEdit(hero)">Edit!</button>\n		<span>{{today | date:\'short\'}}</span>\n	    \n		<span>USER 1</span><span ng-click="remove_send_message1($index)">-Delete-</span>\n	</div></div>\n\n	<div class="person-two common-person" ng-repeat="message_user2 in all_messages_from2">\n	    <div class="message-box">\n\n		<p>{{message_user2}}</p>\n	    \n		<span>USER 2</span><span ng-click="remove_send_message2($index)">-Delete-</span>\n  </div></div>\n  \n\n\n\n\n\n\n\n	<label class="item item-input item-floating-label">\n	    <span class="input-label">type your message here</span>\n	    <input type="text" placeholder="message..." autofocus\n      #todoInput [value]="newItem" (input)="newItemChanged(todoInput.value)">\n	</label>\n\n	<button class="button button-full button-assertive" (click)="send_message1()">SEND</button>\n	<br><br>\n	\n	<label class="item item-input item-floating-label">\n	    <span class="input-label">type your message here</span>\n	    <input type="text" placeholder="USER 2" [(ngModel)]="heroName">\n	</label>\n\n	<button class="button button-full button-assertive" (click)="send_message2()">SEND</button>\n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/chat-window/chat-window.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], ChatWindowPage);
    return ChatWindowPage;
}());
export { ChatWindowPage };
var Hero = /** @class */ (function () {
    function Hero(id, name) {
        this.id = id;
        this.name = name;
    }
    return Hero;
}());
export { Hero };
//# sourceMappingURL=chat-window.js.map