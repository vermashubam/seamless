var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the NewTicketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewTicketPage = /** @class */ (function () {
    function NewTicketPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NewTicketPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewTicketPage');
    };
    NewTicketPage = __decorate([
        Component({
            selector: 'page-new-ticket',template:/*ion-inline-start:"/var/www/html/ionic-conference-app/src/pages/new-ticket/new-ticket.html"*/'<!--\n  Generated template for the NewTicketPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>new-ticket</ion-title>\n  </ion-navbar>\n\n  <div class="client tickets_head newhead"><img src="images/linda-big.png"><h4 class="medi-title medi-titlenew-ticket">New Ticket</h4></div>\n  \n\n</ion-header>\n\n\n<ion-content padding>\n\n    <div class="col-md-12 col-xs-12 new-tickets">\n        <div class="col-xs-12 new-tickets-title" style="padding:0px;">\n        <h3><a href="client-ticket-details.html" style="color:#333333;">What would you like to do?</a></h3>\n        </div>\n        <div class="col-md-12 col-xs-12 text-left new-ticketss">\n        <p><a href="reorder.html">Re-Order Meds / Change Autofill</a></p>\n        <p>Speak with a Pharmacist</p>\n        <p>Request Spares</p>\n        </div>\n        </div>\n        <div class="col-md-12 col-xs-12 new-ticket-bottom">\n          <p>General tion</p>\n        </div>\n        \n        \n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/ionic-conference-app/src/pages/new-ticket/new-ticket.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], NewTicketPage);
    return NewTicketPage;
}());
export { NewTicketPage };
//# sourceMappingURL=new-ticket.js.map